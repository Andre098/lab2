from FS import Log, Binary, Directory, Buffer

def test_add():
  folder = Directory('folder', [])
  log = Log('log', 'logged')
  binary = Binary('bin', '0X12391')
  folder_2 = Directory('folder_2', [])

  folder_2.add(log)
  folder.add(binary)
  folder.add(folder_2)

  assert(folder.add(log))
  assert(folder.get_file('log', '.log') == log)
  assert(folder.get_file('bin', '.bin') == binary)


def test_delete():
  folder = Directory('folder', [])
  log = Log('log', 'logged')
  folder_2 = Directory('folder_2', [])

  folder_2.add(log)
  folder.add(folder_2)

  assert(folder.delete(log) == False)
  assert(folder.delete(folder_2))


def test_move():
  folder = Directory('folder', [])
  log = Log('log', 'logged')
  move_to = Directory('move_to', [])

  folder.add(log)
  folder.add(move_to)

  assert(folder.move(log, move_to))
  assert(move_to.get_file('log', '.log') == log)


def test_size():
  folder = Directory('folder', [])
  for i in range(12):
    folder.add(Log('log'+str(i), 'logged'))
  assert(folder.add(Log('log12', 'logged')) == False)

# Log

def test_read_file():
  log = Log('log', 'some text')
  text = log.read_file()

  assert text == 'some text'

def test_append_line():
  log = Log('log', 'some text')
  log.append_line('new line')
  text = log.read_file()
  
  assert text == 'some text\nnew line'

# Buffer

def test_push():
  buffer = Buffer('buffer', [])
  assert(buffer.push('ff'))

  buffer = Buffer('buffer', ['11', '22', '33', '44'])
  assert(buffer.push('55') == False)

def test_consume():
  buffer = Buffer('buffer', [])
  buffer.push('ff')
  assert(buffer.consume())
  assert(buffer.consume() == False)

# Binary

def test_read_file():
  binary = Binary('bin', '00')
  text = binary.read_file()
  assert text == '00'