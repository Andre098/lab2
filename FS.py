class Directory():
  extension = '/'
  DIR_MAX_ELEMS = 12
  children = []

  def __init__(self, name, children):
    self.name = name
    self.children = children

  def add(self, element):
    if (len(self.children) >= self.DIR_MAX_ELEMS):
      print('Reached the limit og elements in folder')
      return False
    else: 
      self.children.append(element)
      element.parent = self
      return True

  def delete(self, element):
    if (element not in self.children):
      print('No such file or folder was found')
      return False
    else:
      self.children.remove(element)
      return True

  def move(self, element, location):
    if(location.extension != '/'):
      print('You can move only to another folder')
      return False
    elif(location.add(element)):
      self.delete(element)
      return True
    else:
      return False

  def get_name(self):
    for x in self.children:
      print(f'{x.name}{x.extension}')

  def list(self, level = 0):
    for x in self.children:
      print(level * '-' + f'{x.name}{x.extension}')
      if (type(x).__name__ == 'Directory'):
        level + 1
        x.get_name()
  
  def get_file(self, name, extension):
    for element in self.children:
      if(element.name == name and element.extension == extension):
        return element
      if(element.extension == '/'):
        res = element.get_file(name, extension)
        if(res != False):
          return res
    return False

class Log():
  extension = '.log'
  content = ''

  def __init__(self, name, content):
    self.name = name
    self.content = content

  def read_file(self):
    return f'{self.content}'

  def append_line(self, line):
    self.content += f'\n{line}'

class Buffer():
  extension = '.buf'
  MAX_BUF_FILE_SIZE = 4
  queue = []

  def __init__(self, name, elements):
    self.name = name
    self.queue = elements

  def push(self, element):
    if(len(self.queue) >= self.MAX_BUF_FILE_SIZE):
      print("Reached the buffer size limit")
      return False
    else:
      self.queue.append(element)
      return True

  def consume(self):
    if(len(self.queue) > 0):
      self.queue.pop()
      return True
    return False

class Binary():
  extension = '.bin'
  content = ''

  def __init__(self, name, content):
    self.name = name
    self.content = content

  def read_file(self):
    return f'{self.content}'