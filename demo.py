from FS import Directory, Log, Binary, Buffer

main = Directory('main', [])

binary = Binary('bin', '00')
log = Log('log', 'log')
buf = Buffer('buf', [])
sub = Directory('sub', []) 

folder_1 = Directory('1', [binary])
folder_2 = Directory('2', [log, buf])
folder_3 = Directory('3', [sub])

main.add(folder_1)
main.add(folder_2)
main.add(folder_3)

print(main.list())
print('>>>>>>>>>>>>>>>>>>>')

folder_1.delete(binary)
folder_2.move(buf, folder_3)
main.delete(folder_1)
print(main.list())
print('>>>>>>>>>>>>>>>>>>>')

file_log = folder_2.get_file('log', '.log')
file_log.append_line('AAAAAAAAAAAA')
print(file_log.read_file())
print('>>>>>>>>>>>>>>>>>>>')

file_buff = folder_3.get_file('buf', '.buf')
file_buff.push('00')
file_buff.push('ff')
file_buff.consume()